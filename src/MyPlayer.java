import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 9, this.playerNumber, -100, 100);


        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber, double alpha, double beta) {

        Move bestMove = new Move(0);
        bestMove.value = Integer.MIN_VALUE;

        int alternating = 1;

        for (int c = 0; c < Board.BOARD_SIZE && bestMove.value < beta; c++){


            int i = 0;
            if(alternating == 1) {
                i = (3 + (c / 2)) % 7;

                alternating = 0;
            }
            else {
                if(c == 5) {
                    i = 0;
                }
                else {
                    i = ((3 + c) % 7) - (c + 1) - (c / 2);
                }

                alternating = 1;
            }


            if (gameBoard.isColumnOpen(i)) {
                Move currentMove = new Move(i);

                gameBoard.move(playerNumber, i);

                int gameStatus = gameBoard.checkIfGameOver(i);
                if (gameStatus >= 0) {

                    if (gameStatus == 0) {
                        // Tie game
                        currentMove.value = 0.0;
                    } else if (gameStatus == playerNumber) {
                        // Win
                        currentMove.value = 1.0;
                    } else {
                        // Loss
                        currentMove.value = -1.0;
                    }

                } else if (maxDepth >= 1) {
                    Move responseMove = search(gameBoard, maxDepth - 1, (playerNumber == 1 ? 2 : 1), -beta, -alpha);
                    currentMove.value = -responseMove.value;

                } else {
                    currentMove.value = heuristic(gameBoard, playerNumber);
                }


                if (currentMove.value > bestMove.value) {
                    bestMove = currentMove;
                    alpha = Math.max(bestMove.value, alpha);
                }

                gameBoard.undoMove(i);
            }

        }

        return bestMove;
    }

    public double heuristic(Board gameBoardB, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

        int[][] gameBoard = gameBoardB.getBoard();
        double returnDoub = 0.0;


        int notPlayerNumber = 0;

        if (playerNumber == 1) {
            notPlayerNumber = 2;
        }
        else {
            notPlayerNumber = 1;
        }



        int rowScore = 0;
        int maxRowScore = 0;

        int rowScoreBad = 0;
        int maxRowScoreBad = 0;

        for(int c = 0; c < 7; c++) {
            for(int q = 0; q < 4; q++) {

                if(gameBoard[c][q] == playerNumber) {
                    rowScore++;

                    if(gameBoard[c][q + 1] == playerNumber) {
                        rowScore++;

                        if(gameBoard[c][q + 2] == playerNumber) {
                            rowScore++;

                            if(gameBoard[c][q + 3] == playerNumber) {

                                rowScore++;

                            }
                            else if(gameBoard[c][q + 3] == notPlayerNumber){
                                rowScore = 0;
                            }
                        }
                        else if(gameBoard[c][q + 2] == 0 && (c == 0 || (gameBoard[c - 1][q + 2] != 0))) {

                            if(gameBoard[c][q + 3] == playerNumber) {
                                rowScore++;
                            }
                            else if(gameBoard[c][q + 3] == notPlayerNumber){
                                rowScore = 0;
                            }
                        }
                        else {
                            rowScore = 0;
                        }
                    } else if((gameBoard[c][q + 1] == 0) && (c == 0 || (gameBoard[c - 1][q + 1] != 0))) {

                        if(gameBoard[c][q + 2] == playerNumber) {
                            rowScore++;

                            if(gameBoard[c][q + 3] == playerNumber) {
                                rowScore++;
                            }
                            else if(gameBoard[c][q + 3] == notPlayerNumber) {
                                rowScore = 0;
                            }
                        }
                        else if((gameBoard[c][q + 2] == 0) && (c == 0 || (gameBoard[c - 1][q + 2] != 0))) {

                            if(gameBoard[c][q + 3] == playerNumber) {
                                rowScore++;
                            }
                            else if(gameBoard[c][q + 3] == notPlayerNumber) {
                                rowScore = 0;
                            }
                        }
                        else {
                            rowScore = 0;
                        }
                    }
                    else {
                        rowScore = 0;
                    }
                } else {
                    rowScore = 0;
                }

                if(rowScore > maxRowScore) {
                    maxRowScore = rowScore;
                }

                rowScore = 0;

                if(gameBoard[c][q] == notPlayerNumber) {
                    rowScoreBad++;

                    if(gameBoard[c][q + 1] == notPlayerNumber) {
                        rowScoreBad++;

                        if(gameBoard[c][q + 2] == notPlayerNumber) {
                            rowScoreBad++;

                            if(gameBoard[c][q + 3] == notPlayerNumber) {

                                rowScoreBad++;

                            }
                            else if(gameBoard[c][q + 3] == playerNumber){
                                rowScoreBad = 0;
                            }
                        }
                        else if(gameBoard[c][q + 2] == 0 && (c == 0 || (gameBoard[c - 1][q + 2] != 0))) {

                            if(gameBoard[c][q + 3] == notPlayerNumber) {
                                rowScoreBad++;
                            }
                            else if(gameBoard[c][q + 3] == playerNumber){
                                rowScoreBad = 0;
                            }
                        }
                        else {
                            rowScoreBad = 0;
                        }
                    }
                    else if(gameBoard[c][q + 1] == 0 && (c == 0 || (gameBoard[c - 1][q + 1] != 0))) {

                        if(gameBoard[c][q + 2] == notPlayerNumber) {
                            rowScoreBad++;

                            if(gameBoard[c][q + 3] == notPlayerNumber) {
                                rowScoreBad++;
                            }
                            else if(gameBoard[c][q + 3] == playerNumber) {
                                rowScoreBad = 0;
                            }
                        }
                        else if(gameBoard[c][q + 2] == 0 && (c == 0 || (gameBoard[c - 1][q + 2] != 0))) {

                            if(gameBoard[c][q + 3] == notPlayerNumber) {
                                rowScoreBad++;
                            }
                            else if(gameBoard[c][q + 3] == playerNumber) {
                                rowScoreBad = 0;
                            }
                        }
                        else {
                            rowScoreBad = 0;
                        }
                    }
                    else {
                        rowScoreBad = 0;
                    }
                }
                else {
                    rowScoreBad = 0;
                }

                if(rowScoreBad > maxRowScoreBad) {
                    maxRowScoreBad = rowScoreBad;
                }

                rowScoreBad = 0;
            }
        }

        int diagScoreUp = 0;
        int diagScoreDown = 0;
        int maxDiagScore = 0;

        int diagScoreUpBad = 0;
        int diagScoreDownBad = 0;
        int maxDiagScoreBad = 0;


        for(int c = 0; c < 7; c++) {
            for(int q = 0; q < 4; q++) {

                if (gameBoard[c][q] == playerNumber) {

                    if (c == 3) {

                        if (gameBoard[c + 1][q + 1] == playerNumber) {
                            diagScoreUp++;

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagScoreUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {

                                    diagScoreUp++;

                                }
                                else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                }
                                else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else {
                                diagScoreUp = 0;
                            }
                        } else if ((gameBoard[c + 1][q + 1] == 0) && (gameBoard[c][q + 1] != 0)) {

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagScoreUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else {
                                diagScoreUp = 0;
                            }
                        } else {
                            diagScoreUp = 0;
                        }


                        if (gameBoard[c - 1][q + 1] == playerNumber) {
                            diagScoreDown++;

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagScoreDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {

                                    diagScoreDown++;

                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else {
                                diagScoreDown = 0;
                            }
                        } else if ((gameBoard[c - 1][q + 1] == 0) && (gameBoard[c - 2][q + 1] != 0)) {

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagScoreDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else {
                                diagScoreDown = 0;
                            }
                        } else {
                            diagScoreDown = 0;
                        }


                    } else if (c < 3) {

                        if (gameBoard[c + 1][q + 1] == playerNumber) {
                            diagScoreUp++;

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagScoreUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {

                                    diagScoreUp++;

                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else {
                                diagScoreUp = 0;
                            }
                        } else if ((gameBoard[c + 1][q + 1] == 0) && (gameBoard[c][q + 1] != 0)) {

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagScoreUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUp++;
                                } else if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUp = 0;
                                }
                            } else {
                                diagScoreUp = 0;
                            }
                        } else {
                            diagScoreUp = 0;
                        }

                    } else {

                        if (gameBoard[c - 1][q + 1] == playerNumber) {
                            diagScoreDown++;

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagScoreDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {

                                    diagScoreDown++;

                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else {
                                diagScoreDown = 0;
                            }
                        } else if ((gameBoard[c - 1][q + 1] == 0) && (gameBoard[c - 2][q + 1] != 0)) {

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagScoreDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDown++;
                                } else if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDown = 0;
                                }
                            } else {
                                diagScoreDown = 0;
                            }
                        } else {
                            diagScoreDown = 0;
                        }
                    }
                }

                if (diagScoreDown > maxDiagScore) {
                    maxDiagScore = diagScoreDown;
                }

                if (diagScoreUp > maxDiagScore) {
                    maxDiagScore = diagScoreUp;
                }

                diagScoreUp = 0;
                diagScoreDown = 0;

                if (gameBoard[c][q] == notPlayerNumber) {

                    if (c == 3) {

                        if (gameBoard[c + 1][q + 1] == notPlayerNumber) {
                            diagScoreUpBad++;

                            if (gameBoard[c + 2][q + 2] == notPlayerNumber) {
                                diagScoreUpBad++;

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {

                                    diagScoreUpBad++;

                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else if ((gameBoard[c][q + 2] == 0) && (gameBoard[c - 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else {
                                diagScoreUpBad = 0;
                            }
                        } else if ((gameBoard[c + 1][q + 1] == 0) && (gameBoard[c][q + 1] != 0)) {

                            if (gameBoard[c + 2][q + 2] == notPlayerNumber) {
                                diagScoreUpBad++;

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else {
                                diagScoreUpBad = 0;
                            }
                        } else {
                            diagScoreUpBad = 0;
                        }


                        if (gameBoard[c - 1][q + 1] == notPlayerNumber) {
                            diagScoreDownBad++;

                            if (gameBoard[c - 2][q + 2] == notPlayerNumber) {
                                diagScoreDownBad++;

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {

                                    diagScoreDownBad++;

                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else {
                                diagScoreDownBad = 0;
                            }
                        } else if ((gameBoard[c - 1][q + 1] == 0) && (gameBoard[c - 2][q + 1] != 0)) {

                            if (gameBoard[c - 2][q + 2] == notPlayerNumber) {
                                diagScoreDownBad++;

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else {
                                diagScoreDownBad = 0;
                            }
                        } else {
                            diagScoreDownBad = 0;
                        }


                    } else if (c < 3) {

                        if (gameBoard[c + 1][q + 1] == notPlayerNumber) {
                            diagScoreUpBad++;

                            if (gameBoard[c + 2][q + 2] == notPlayerNumber) {
                                diagScoreUpBad++;

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {

                                    diagScoreUpBad++;

                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else {
                                diagScoreUpBad = 0;
                            }
                        } else if ((gameBoard[c + 1][q + 1] == 0) && (gameBoard[c][q + 1] != 0)) {

                            if (gameBoard[c + 2][q + 2] == notPlayerNumber) {
                                diagScoreUpBad++;

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else if ((gameBoard[c + 2][q + 2] == 0) && (gameBoard[c + 1][q + 2] != 0)) {

                                if (gameBoard[c + 3][q + 3] == notPlayerNumber) {
                                    diagScoreUpBad++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagScoreUpBad = 0;
                                }
                            } else {
                                diagScoreUpBad = 0;
                            }
                        } else {
                            diagScoreUpBad = 0;
                        }

                    } else {

                        if (gameBoard[c - 1][q + 1] == notPlayerNumber) {
                            diagScoreDownBad++;

                            if (gameBoard[c - 2][q + 2] == notPlayerNumber) {
                                diagScoreDownBad++;

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {

                                    diagScoreDownBad++;

                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else {
                                diagScoreDownBad = 0;
                            }
                        } else if ((gameBoard[c - 1][q + 1] == 0) && (gameBoard[c - 2][q + 1] != 0)) {

                            if (gameBoard[c - 2][q + 2] == notPlayerNumber) {
                                diagScoreDownBad++;

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else if ((gameBoard[c - 2][q + 2] == 0) && (gameBoard[c - 3][q + 2] != 0)) {

                                if (gameBoard[c - 3][q + 3] == notPlayerNumber) {
                                    diagScoreDownBad++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagScoreDownBad = 0;
                                }
                            } else {
                                diagScoreDownBad = 0;
                            }
                        } else {
                            diagScoreDownBad = 0;
                        }
                    }
                }

                if (diagScoreDownBad > maxDiagScore) {
                    maxDiagScoreBad = diagScoreDownBad;
                }

                if (diagScoreUpBad > maxDiagScore) {
                    maxDiagScoreBad = diagScoreUpBad;
                }

                diagScoreUpBad = 0;
                diagScoreDownBad = 0;
            }
        }

        int columnScore = 0;
        int maxColumnScore = 0;

        int columnScoreBad = 0;
        int maxColumnScoreBad = 0;

        for(int t = 0; t < 4; t++) {
            for(int f = 0; f < 7; f++) {
                if(gameBoard[t][f] == playerNumber) {
                    columnScore++;

                    if(gameBoard[t + 1][f] == playerNumber) {
                        columnScore++;

                        if(gameBoard[t + 2][f] == playerNumber) {
                            columnScore++;

                            if(gameBoard[t + 3][f] == playerNumber) {

                                columnScore++;

                            }
                            else if(gameBoard[t + 3][f] == notPlayerNumber){
                                columnScore = 0;
                            }
                        }
                        else if(gameBoard[t + 2][f] == notPlayerNumber){
                            columnScore = 0;
                        }
                    }
                    else if(gameBoard[t + 1][f] == notPlayerNumber) {
                        columnScore = 0;
                    }
                }
                else if(gameBoard[t][f] == notPlayerNumber){
                    columnScore = 0;
                }

                if(columnScore > maxColumnScore) {
                    maxColumnScore = columnScore;
                }

                columnScore = 0;

                if(gameBoard[t][f] == notPlayerNumber) {
                    columnScoreBad++;

                    if(gameBoard[t + 1][f] == notPlayerNumber) {
                        columnScoreBad++;

                        if(gameBoard[t + 2][f] == notPlayerNumber) {
                            columnScoreBad++;

                            if(gameBoard[t + 3][f] == notPlayerNumber) {

                                columnScoreBad++;

                            }
                            else if(gameBoard[t + 3][f] == playerNumber){
                                columnScoreBad = 0;
                            }
                        }
                        else if(gameBoard[t + 2][f] == playerNumber){
                            columnScoreBad = 0;
                        }
                    }
                    else if(gameBoard[t + 1][f] == playerNumber) {
                        columnScoreBad = 0;
                    }
                }
                else if(gameBoard[t][f] == playerNumber){
                    columnScoreBad = 0;
                }

                if(columnScoreBad > maxColumnScoreBad) {
                    maxColumnScoreBad = columnScoreBad;
                }

                columnScoreBad = 0;

                if((gameBoard[t][f] == notPlayerNumber) && (t == 3)) {
                    maxColumnScore = 0;
                    maxColumnScoreBad = 9;

                    if(gameBoard[t + 1][f] == playerNumber) {
                        maxColumnScoreBad = 0;
                    }

                    if(gameBoard[t + 2][f] == playerNumber) {
                        maxColumnScoreBad = 0;
                    }

                    if(gameBoard[t + 3][f] == playerNumber) {
                        maxColumnScoreBad = 0;
                    }
                }
            }
        }

        returnDoub = ((maxColumnScore * maxColumnScore + maxRowScore * maxRowScore + maxDiagScore * maxDiagScore) - (maxColumnScoreBad * maxColumnScoreBad + maxRowScoreBad * maxRowScoreBad + maxDiagScoreBad * maxDiagScoreBad)) / 100.0;


        return returnDoub;
    }


}
